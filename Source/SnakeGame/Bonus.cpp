// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"

void ABonus::SetTimeToDestroy()
{
	bTimeToDestroy = true;
}

// Sets default values
ABonus::ABonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();

	//changes Destroy flag to true after 3 sec
	bTimeToDestroy = false;
	GetWorld()->GetTimerManager().SetTimer(KillBonusTimer, this, &ABonus::SetTimeToDestroy, 3.f, false);
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//ostillating 
	FVector Scale(FMath::Abs( FMath::Sin(GetWorld()->GetRealTimeSeconds() * 5) ) + 3.f);
	this->SetActorScale3D(Scale / 4);

	//destroy Bonus and spawn a new one if time is out
	if (bTimeToDestroy) 
	{
		ASnakeBase* Snake = (ASnakeBase*)UGameplayStatics::GetActorOfClass(GetWorld(), ASnakeBase::StaticClass());
		if (Snake)
		{
			GetWorld()->SpawnActor<ABonus>(NewBonus, FTransform(Snake->LocationRandomizer()));
		}
		this->Destroy();
	}
}


void ABonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();

			Snake->MovementSpeed = 0.5f;
			if (Snake)
			{
				GetWorld()->SpawnActor<ABonus>(NewBonus, FTransform(Snake->LocationRandomizer()));
			}

			this->Destroy();
		}
	}
}

