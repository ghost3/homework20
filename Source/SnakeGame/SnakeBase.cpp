// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "YouDied.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(3);
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	SetActorTickInterval(MovementSpeed);
	GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Yellow, FString::Printf(TEXT("MovementSpeed is %f"), MovementSpeed)); // Display current speed every snake's tick

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, -1000); // spawn new element far far away to avoid it's strange apperance on a field
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		//NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0) // returns head back to field after spawning far far away
		{
			FVector NewLocation2(SnakeElements.Num() * ElementSize, 0, 0); 
			NewSnakeElem->SetActorLocation(NewLocation2);
			NewSnakeElem->SetFirstElementType();
		}
	}

	GEngine->AddOnScreenDebugMessage(-2, 9.f, FColor::Green, FString::Printf(TEXT("Snake length is %d"), SnakeElements.Num())); // Display number of elements every incremention of elements

}

void ASnakeBase::Move() // move controls
{
	FVector MovementVector(ForceInitToZero);
	float MovementStep = ElementSize; //ex MovementSpeed	
	


	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementStep;
		UE_LOG(LogTemp, Warning, TEXT("Move up"));
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementStep;
		UE_LOG(LogTemp, Warning, TEXT("Move down"));
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementStep;
		UE_LOG(LogTemp, Warning, TEXT("Move left"));
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementStep;
		UE_LOG(LogTemp, Warning, TEXT("Move right"));
		break;
	}



	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();	//collision off

	// move tail logic
	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		//FVector PrevLocation = PrevElement->GetActorLocation(); // ex
		FVector PrevLocation(PrevElement->GetActorLocation().X, PrevElement->GetActorLocation().Y, 0); // z to 0 is attempt to solve incorrect block spawn
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();	//collision on
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0; 
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SpawnFood()
{
}

void ASnakeBase::SpawnBonus()
{
}

FVector ASnakeBase::LocationRandomizer()
{
	return FVector(FMath::RandRange(-9, 9) * ElementSize, FMath::RandRange(-9, 9) * ElementSize, 0);
}

void ASnakeBase::SpawnEndgameScreen()
{
	FTransform NewTransform(FVector(0, 0, 200));
	AYouDied* EndgameScreen = GetWorld()->SpawnActor<AYouDied>(EndgameScreenClass, NewTransform);
}


